package com.jtc;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

@ManagedBean(name="formController")
@SessionScoped
public class FormController {
	
	@ManagedProperty(value="#{greetingBean}")
	private GreetingBean greetingValue;
	
	public void processFormValues(GreetingBean bean){
		System.out.println(bean.getName());
	}
	
	public GreetingBean getGreetingValue() {
		return greetingValue;
	}

	public void setGreetingValue(GreetingBean greetingValue) {
		this.greetingValue = greetingValue;
	}
}
